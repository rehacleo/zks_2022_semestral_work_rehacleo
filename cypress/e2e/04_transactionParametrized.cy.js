import {acceptCookies, clearUser, openWallet, openWebsite} from "../helpers/base.cy";
import dashboardPage from "../pages/DashboardPage";
import transactionPage from "../pages/TransactionPage";
import {getCurrentBalance, selectCategory} from "../helpers/transactionHelper.cy";
import dayjs from "dayjs";
import {login} from "../helpers/login.cy";

describe('Transaction parametrized', () => {

    let balance = 0;
    let init = true;

    before(() => {
        clearUser();
        openWebsite();
        acceptCookies();

        login();

        openWallet();

        cy.url().should('contain', 'transactions')

        transactionPage.actualBalance.should('be.visible')

        cy.wait(2000)

        getCurrentBalance().then(value => {
            balance = value;
        });
    })

    it('should add all transactions', function () {

        cy.fixture('Transactions').then(transactions => {

            for (let transaction of transactions) {

                transactionPage.addNewTranBtn.click()

                if (init) {
                    transactionPage.addNewTranBtn.click()
                }

                if (transaction.type === "income") {
                    transactionPage.incomeButton.click();
                } else {
                    transactionPage.expensesButton.click();
                }

                selectCategory(transaction.category).click()
                transactionPage.dateInput.clear()
                const currentDate = dayjs().format('DD/MM/YYYY');
                transactionPage.dateInput.type(currentDate);
                transactionPage.noteInput.type(transaction.note);
                transactionPage.priceInput.type(transaction.price);
                transactionPage.addTranBtn.click();

                init = false;

                getCurrentBalance().then(value => {
                    expect(balance + Number(transaction.price)).eq(value);
                    balance = value;
                });

                cy.wait(2000);
            }
        })
    });

    after(() => {
        transactionPage.transactionCheckbox.click({multiple: true})

        transactionPage.deleteBtn.click()

        transactionPage.confirmDeleteBtn.click()

        getCurrentBalance().then(value => {
            expect(0).eq(value);
            balance = value;
        });
    })

})