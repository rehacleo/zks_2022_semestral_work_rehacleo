import {acceptCookies, clearUser, openWebsite} from "../helpers/base.cy";
import dashboardPage from "../pages/DashboardPage";
import {login} from "../helpers/login.cy";

describe("Dashboard", () => {



    beforeEach(() => {
        clearUser();
        openWebsite();
        acceptCookies();

        login();
    })

    it('should get wallet name', function () {
        const walletName = "Testing Wallet";

        dashboardPage.WalletName.should('contain.text', walletName);

    });

    it('should get username', function () {

        const username = "Testing  Account"

        dashboardPage.username.should('contain.text', username)

    });

})