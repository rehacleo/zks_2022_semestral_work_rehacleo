import {acceptCookies, clearUser, openWaller, openWallet, openWebsite} from "../helpers/base.cy";
import {login} from "../helpers/login.cy";
import dashboardPage from "../pages/DashboardPage";
import transactionPage from "../pages/TransactionPage";
import {getCurrentBalance, selectCategory} from "../helpers/transactionHelper.cy";
import dayjs from "dayjs";

describe('Transactions', () => {

    let balance = 0;

    beforeEach(() => {
        clearUser();
        openWebsite();
        acceptCookies();
        login()

        openWallet();

        cy.url().should('contain', 'transactions')

        transactionPage.actualBalance.should('be.visible')

        cy.wait(2000)
    })

    it('should get current balance', function () {
        getCurrentBalance().then(value => {
            balance = value;
        });
    });

    it('should add Income transaction', function () {

        transactionPage.addNewTranBtn.click()

        if (balance === 0){
            transactionPage.addNewTranBtn.click()
        }

        transactionPage.incomeButton.click()

        selectCategory('Plat').click()

        transactionPage.dateInput.clear()

        const currentDate = dayjs().format('DD/MM/YYYY');

        transactionPage.dateInput.type(currentDate);

        transactionPage.noteInput.type('payout');

        transactionPage.priceInput.type('10000');

        transactionPage.addTranBtn.click();

        getCurrentBalance().then(value => {
            expect(balance+10000).eq(value);
            balance = value;
        });
    });

    it('should add Expenses transaction', function () {

        transactionPage.addNewTranBtn.click()

        transactionPage.expensesButton.click()

        selectCategory('Nákupy').click()

        transactionPage.dateInput.clear()

        const currentDate = dayjs().format('DD/MM/YYYY');

        transactionPage.dateInput.type(currentDate);

        transactionPage.noteInput.type('shopping');

        transactionPage.priceInput.type('-300');

        transactionPage.addTranBtn.click();

        getCurrentBalance().then(value => {
            expect(balance-300).eq(value);
            balance = value;
        });
    });

    it('should delete all transactions', function () {

        transactionPage.transactionCheckbox.click({multiple: true})

        transactionPage.deleteBtn.click()

        transactionPage.confirmDeleteBtn.click()

        getCurrentBalance().then(value => {
            expect(0).eq(value);
            balance = value;
        });

    });

})