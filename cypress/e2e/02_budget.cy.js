import {acceptCookies, clearUser, openBudget, openWebsite} from "../helpers/base.cy";
import {login} from "../helpers/login.cy";
import budgetPage from "../pages/BudgetPage";

describe('Budget', () => {
    beforeEach(() => {
        clearUser();
        openWebsite();
        acceptCookies();

        login()

        openBudget();

        cy.wait(2000);
    })

    it('should has no budget', function () {

        budgetPage.budgetList.children().should('have.length', 1);

        budgetPage.newBudgetButton.should('contain.text', 'Create a New Budget');
    });

    it('should create new Food budget', function () {

        cy.wait(5000);

        budgetPage.newBudgetButton.click();

        budgetPage.newBudgetNameInput.type("Food");

        budgetPage.newBudgetAmountInput.type('5000');

        budgetPage.newBudgetDayPeriodInput.click();

        budgetPage.createNewBudgetButton.click();

        budgetPage.budgetList.children().should('have.length', 2);

        budgetPage.budgetName.should('contain.text', 'Food');
    });


    it('should delete budget', function () {

        budgetPage.budgetList.children().should('have.length', 2);

        budgetPage.linkToBudgetDetail.click();

        budgetPage.editBudgetButton.click()

        budgetPage.deleteBudgetButton.click()

        budgetPage.confirmDeleteBudgetButton.click()

        openBudget();

        budgetPage.budgetList.children().should('have.length', 1);

    });


})