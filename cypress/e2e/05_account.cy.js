import {acceptCookies, clearUser, openAccount, openWebsite} from "../helpers/base.cy";
import {EMAIL, login} from "../helpers/login.cy";
import accountPage from "../pages/AccountPage";

describe('Account', () => {
    beforeEach(() => {
        clearUser();
        openWebsite();
        acceptCookies();

        login();
    })

    it('should get account email', function () {
        openAccount();

        cy.url().should('contain', 'account')

        cy.wait(5000);

        accountPage.emailInput.should('contain.value', EMAIL);
    });
})