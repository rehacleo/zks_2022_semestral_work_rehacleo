import {acceptCookies, clearUser, openWebsite} from "../helpers/base.cy";
import loginPage from "../pages/LoginPage";

describe("Test login from fixtures", () => {
    beforeEach(() => {
        clearUser();
        openWebsite();
        acceptCookies();
    })

    it('Should login all valid', function () {
        cy.fixture('ValidLogin').then(logins => {
            logins.forEach(l => {
                loginPage.EmailInput.type(l.email);
                loginPage.PasswordInput.type(l.password);
                loginPage.LoginButton.click();

                cy.url().should('eq','https://app.spendee.com/dashboard');
            })
        })
    });

    it('Should login all invalid', function () {
        cy.fixture('InvalidLogin').then(logins => {
            logins.forEach(l => {
                console.log(l)
                loginPage.EmailInput.type(l.email);
                loginPage.PasswordInput.type(l.password);
                loginPage.LoginButton.click();

                loginPage.ErrorMessage.should('contain.text', l.error)
                cy.wait(5000)
            })
        })
    });
})