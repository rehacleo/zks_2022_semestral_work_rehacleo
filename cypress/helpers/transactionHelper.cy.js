import transactionPage from "../pages/TransactionPage";

export function getCurrentBalance() {
    return transactionPage.actualBalance.then(text => {
        const balText = text.text();
        expect(balText).to.contain('CZK')
        return Number(balText.split(' ')[0].replaceAll(',', ''));
    });
}

export function selectCategory(name){
    return cy.contains('span', name)
}