import loginPage from "../pages/LoginPage";

export const EMAIL = "wifira6055@ezgiant.com";
export const PASSWORD = "TestingAccount";

export function login(email = EMAIL, password = PASSWORD){
    typeLogin(email, password);
    clickLogin();
    cy.url().should('eq','https://app.spendee.com/dashboard');
}


export function typeLogin(email, password){
    loginPage.EmailInput.type(email);
    loginPage.PasswordInput.type(password);

}

export function clickLogin(){
    loginPage.LoginButton.click();
}