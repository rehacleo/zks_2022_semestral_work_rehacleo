export const baseUrl = "https://app.spendee.com/";

export function openWebsite(){
    return cy.visit(baseUrl);
}

export function openWallet(){
    return cy.visit('https://app.spendee.com/wallet/f04ebebe-0003-4ab2-9e3f-27ca718c8025/transactions');
}

export function openBudget(){
    return cy.visit('https://app.spendee.com/wallet/f04ebebe-0003-4ab2-9e3f-27ca718c8025/budgets');
}

export function openAccount(){
    return cy.visit('https://app.spendee.com/settings/account');
}

export function clearUser(){
    cy.clearAllCookies()
    cy.clearAllLocalStorage()
    cy.clearAllSessionStorage()
    cy.clearFirebaseAuth();
}

export function acceptCookies(){
    return cy.contains('Got it').click()
}