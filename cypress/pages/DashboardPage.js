

class DashboardPage{

    get Wallet(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div > section > div > div > div:nth-child(1) > a')
    }

    get WalletName(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div > section > div > div > div:nth-child(1) > a > div > article > header > span._3_HJ > span._1dxB')
    }

    get username(){
        return cy.get('#app > div > div._2dTk._1prg > header > div._2dHu > div > div.uZrZ > div > div:nth-child(1) > div > div._2jFV._1sxG > div > span');
    }

}

export default new DashboardPage();