export class AccountPage{

    get emailInput(){
        return cy.get('[id="email"]');
    }

}

export default new AccountPage();