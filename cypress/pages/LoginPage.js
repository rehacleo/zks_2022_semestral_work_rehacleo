class LoginPage{
    get EmailInput(){
        return cy.get('[id="email"]');
    }

    get PasswordInput(){
        return cy.get('[id="password"]');
    }

    get LoginButton(){
        return cy.get('[type="submit"]');
    }

    get ErrorMessage(){
        return cy.get('._14lC')
    }
}

export default new LoginPage();