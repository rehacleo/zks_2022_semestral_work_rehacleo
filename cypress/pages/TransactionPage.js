export class TransactionPage{

    get addNewTranBtn(){
        return cy.get('.addInverse')
    }

    get actualBalance(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div._2AsN._3EiL > div > div > div:nth-child(1) > div > span._2Tip')
    }

    get incomeButton(){
        return cy.contains('button', 'Income')
    }

    get expensesButton(){
        return cy.contains('button', 'Expenses')
    }

    get dateInput(){
        return cy.get('[placeholder="DD/MM/YYYY"]')
    }

    get noteInput(){
        return cy.get('#note')
    }

    get labelSelect(){
        return cy.get('.Select.Select--multi.is-searchable');
    }

    get priceInput(){
        return cy.get('#price')
    }

    get addTranBtn(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div.UzPn._10vh._3Eyk > div > div > div:nth-child(2) > div._2Rgf._1TEN > div > div > form > div > div.oTaE._3kSa > div.LB4I._1Nfn.JBC3 > div._3fS2.o-cw._2eVd._229f._5CwE._9tQt > button')
    }

    get transactionCheckbox(){
        return cy.get('._4Nff');
    }

    get deleteBtn(){
        return cy.contains('a', 'Delete')
    }

    get confirmDeleteBtn(){
        return cy.get('button').contains('Delete transaction');
    }
}

export default new TransactionPage();