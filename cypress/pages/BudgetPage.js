export class BudgetPage {

    get budgetList(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div > div > div > div.oTaE > div');
    }

    get linkToBudgetDetail(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div > div > div > div.oTaE > div > div:nth-child(1) > a');
    }

    get budgetName(){
        return cy.get('#app > div > div:nth-child(2) > div > div > div > div > div > div.oTaE > div > div:nth-child(1) > a > div > article > h3');
    }

    get newBudgetButton(){
        return cy.get('button').contains('Create a New Budget');
    }

    get newBudgetNameInput(){
        return cy.get('[name="name"]');
    }

    get newBudgetAmountInput(){
        return cy.get('[name="amount"]');
    }

    get newBudgetDayPeriodInput(){
        return cy.get('[value="day"]');
    }

    get createNewBudgetButton(){
        return cy.get('button').contains('Create a Budget')
    }

    get editBudgetButton(){
        return cy.contains('Edit budget');
    }

    get deleteBudgetButton(){
        return cy.contains('Delete budget');
    }

    get confirmDeleteBudgetButton(){
        return cy.get('[class="_3WuR _3SdL _1_Kr"]').contains('Delete budget');
    }

}

export default new BudgetPage();